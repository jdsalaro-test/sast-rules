// License: MIT (c) GitLab Inc.
#include <stdio.h>

void main() {
  printf("Hello!");
}

int demo(char * a, char * b) {
  syslog(LOG_ERR, "cannot open config file (%s): %s", filename, strerror(errno))
  syslog(LOG_CRIT, "malloc() failed");
  /* But this one SHOULD trigger a warning. */
  // ruleid: c_format_rule-syslog
  syslog(LOG_ERR, attacker_string);
}
